require "rails_helper"

RSpec.describe 'Posts', type: :request do
	
	describe 'GET / posts' do
		before { get '/post' } # generar una peticion 

		it 'should return ok' do
			payload = JSON.parse(response.body)
			expect(payload).to be_empty # si no tenemos ningun post en la db.
			expect(response).to have_http_status(200)
		end
		
	end
	
	describe 'with data in DB'do
		before { get '/post' } # generar una peticion 
		
		let(:posts) { create_list(:post, 10, published: true)}
		#let permite crear una variable que puede usarse en las pruebas siguientes (rspec)
		# createlist metodo de factory bot
		it 'should return all the published posts' do
			payload = JSON.parse(response.body)
			expect(payload.size).to eq(posts.size)
			expect(response).to have_http_status(200)
		end
	end		

	describe 'GET /post/{id}' do
		let(:post) { create(:post) }
		
		it 'should return a post' do
			get "/post/#{post.id}" # generar una peticion dentro de la prueba para tener acceso a la variable post
			#asi poder usar el id
			payload = JSON.parse(response.body)
			expect(payload).to_not be_empty			
			expect(payload["id"]).to eq(post.id)
			expect(response).to have_http_status(200)
		end
	end

end
	
